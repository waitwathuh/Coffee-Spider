package com.cs.annotations;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.cs.enums.HttpMethod;
import com.cs.http.BasicHttpRequest;

public class AnnotationFinder
{
	private final String PACKAGE_NAME = "";

	private ClassLoader classLoader;
	private Enumeration< URL > resources;
	private List< File > dirs;
	private List< Class< ? > > classes;
	private List< Method > methodsWithGetRequestMappingAnnotation;
	private List< Method > methodsWithPostRequestMappingAnnotation;

	public AnnotationFinder()
	{
		String[] classPath = System.getProperty( "java.class.path" ).split( System.getProperty( "path.separator" ) );

		if ( classPath[ 0 ].endsWith( ".jar" ) )
		{
			runningJar();
		}
		else
		{
			runningIDE();
		}
	}

	private void runningIDE()
	{
		try
		{
			initializeObjects();
			getAllSubDirectories();
			getAllClassesInPackage();
			getAllClassesWithCustomAnnotation();
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}
		catch ( URISyntaxException e )
		{
			e.printStackTrace();
		}
		catch ( ClassNotFoundException e )
		{
			e.printStackTrace();
		}
	}

	private void runningJar()
	{
		try
		{
			initializeObjects();
			getAllClassesInJar();
			getAllClassesWithCustomAnnotation();
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}
		catch ( ClassNotFoundException e )
		{
			e.printStackTrace();
		}
	}

	private void initializeObjects() throws IOException
	{
		classLoader = Thread.currentThread().getContextClassLoader();
		resources = classLoader.getResources( PACKAGE_NAME.replace( '.', '/' ) );
		dirs = new ArrayList< File >();
		classes = new ArrayList< Class< ? > >();
		methodsWithGetRequestMappingAnnotation = new ArrayList< Method >();
		methodsWithPostRequestMappingAnnotation = new ArrayList< Method >();
	}

	private void getAllSubDirectories() throws URISyntaxException
	{
		while ( resources.hasMoreElements() )
		{
			try
			{
				URL resource = resources.nextElement();
				URI uri = new URI( resource.toString() );
				dirs.add( new File( uri.getPath() ) );
			}
			catch ( Exception ex )
			{
				System.out.println( ex.getMessage() );
			}
		}
	}

	private void getAllClassesInJar() throws IOException, ClassNotFoundException
	{
		for ( String classpathEntry : System.getProperty( "java.class.path" ).split( System.getProperty( "path.separator" ) ) )
		{
			if ( classpathEntry.endsWith( ".jar" ) )
			{
				JarFile jar = new JarFile( classpathEntry );

				for ( Enumeration< JarEntry > entries = jar.entries(); entries.hasMoreElements(); )
				{
					String file = entries.nextElement().getName();

					if ( file.endsWith( ".class" ) )
					{
						String classname = file.replace( '/', '.' ).substring( 0, file.length() - 6 );
						classes.add( Class.forName( classname ) );
					}
				}

				jar.close();
			}
		}
	}

	private void getAllClassesInPackage() throws ClassNotFoundException
	{
		for ( File directory : dirs )
		{
			findClassesInDirectory( directory, PACKAGE_NAME );
		}
	}

	private void findClassesInDirectory( File directory, String packageName )
	{
		try
		{
			validateDirectory( directory );
			loopThroughFilesInDirectory( directory.listFiles(), packageName );
		}
		catch ( IOException e )
		{
			e.printStackTrace();
		}
		catch ( ClassNotFoundException e )
		{
			e.printStackTrace();
		}
	}

	private void validateDirectory( File directory ) throws IOException
	{
		if ( directory.exists() == false )
		{
			throw new IOException();
		}
	}

	private void loopThroughFilesInDirectory( File[] filesInDirectory, String packageName ) throws ClassNotFoundException
	{
		for ( File file : filesInDirectory )
		{
			if ( file.isDirectory() )
			{
				processDirectory( file, packageName );
			}
			else if ( file.getName().endsWith( ".class" ) )
			{
				processFile( file, packageName );
			}
		}
	}

	private void processDirectory( File file, String packageName ) throws ClassNotFoundException
	{
		if ( packageName.equals( "" ) )
		{
			findClassesInDirectory( file, file.getName() );
		}
		else
		{
			findClassesInDirectory( file, packageName + "." + file.getName() );
		}
	}

	private void processFile( File file, String packageName ) throws ClassNotFoundException
	{
		if ( packageName.equals( "" ) )
		{
			String str = file.getName().substring( 0, file.getName().length() - 6 );
			classes.add( Class.forName( str ) );
		}
		else
		{
			String str = packageName + '.' + file.getName().substring( 0, file.getName().length() - 6 );
			classes.add( Class.forName( str ) );
		}
	}

	private void getAllClassesWithCustomAnnotation()
	{
		for ( Class< ? > singleClass : classes )
		{
			if ( singleClass.isAnnotationPresent( GetRequest.class ) )
			{
				for ( Method method : singleClass.getMethods() )
				{
					if ( method.isAnnotationPresent( Mapping.class ) )
					{
						methodsWithGetRequestMappingAnnotation.add( method );
					}
				}
			}
			else if ( singleClass.isAnnotationPresent( PostRequest.class ) )
			{
				for ( Method method : singleClass.getMethods() )
				{
					if ( method.isAnnotationPresent( Mapping.class ) )
					{
						methodsWithPostRequestMappingAnnotation.add( method );
					}
				}
			}
		}
	}

	public Method getMethod( BasicHttpRequest request ) throws NoSuchMethodException
	{
		Method returnMethod = null;

		if ( request.getHttpMethod().equals( HttpMethod.GET ) )
		{
			for ( Method method : methodsWithGetRequestMappingAnnotation )
			{
				returnMethod = fixThis( request, method.getAnnotation( Mapping.class ).path(), method );

				if ( returnMethod != null )
					return returnMethod;
			}
		}
		else if ( request.getHttpMethod().equals( HttpMethod.POST ) )
		{
			for ( Method method : methodsWithPostRequestMappingAnnotation )
			{
				returnMethod = fixThis( request, method.getAnnotation( Mapping.class ).path(), method );

				if ( returnMethod != null )
					return returnMethod;
			}
		}

		throw new NoSuchMethodException();
	}

	// TODO - Clean up this method
	private Method fixThis( BasicHttpRequest request, String requestMapping, Method method )
	{
		if ( request.getRequestUri().equals( requestMapping ) )
		{
			return method;
		}
		else if ( requestMapping.contains( "{" ) && requestMapping.contains( "}" ) )
		{
			String[] requestParts = request.getRequestUri().split( "/" );
			String[] methodParts = requestMapping.split( "/" );
			LoL: // TODO - Should not use GoTo

			if ( requestParts.length == methodParts.length )
			{
				for ( int i = 1; i < requestParts.length; i++ )
				{
					if ( requestParts[ i ].equals( methodParts[ i ] ) == false && methodParts[ i ].contains( "{" ) == false && methodParts[ i ].contains( "}" ) == false )
					{
						break LoL;
					}
				}

				addPathParams( request, requestParts, methodParts );
				return method;
			}
		}

		return null;
	}

	private void addPathParams( BasicHttpRequest request, String[] requestParts, String[] methodParts )
	{
		List< String > pathParamList = new ArrayList<>();

		for ( int i = 1; i < methodParts.length; i++ )
		{
			if ( methodParts[ i ].contains( "{" ) && methodParts[ i ].contains( "}" ) )
			{
				pathParamList.add( requestParts[ i ] );
			}
		}

		String[] pathParamArray = pathParamList.toArray( new String[ 0 ] );
		request.setPathParams( pathParamArray );
	}
}
package com.cs.http;

import com.cs.enums.HttpMethod;
import com.cs.interfaces.HttpRequest;
import com.cs.interfaces.Logger;

public class BasicHttpRequest extends BasicHttpMessage implements HttpRequest
{
	private HttpMethod httpMethod;
	private String requestUri;
	private String[] pathParams = new String[0];

	public BasicHttpRequest( Logger logger )
	{
		super( logger );
	}

	@Override
	public HttpMethod getHttpMethod()
	{
		return httpMethod;
	}

	@Override
	public String getRequestUri()
	{
		return requestUri;
	}

	public void setHttpMethod( HttpMethod httpMethod )
	{
		this.httpMethod = httpMethod;
	}
	
	public void setRequestUri( String requestUri )
	{
		this.requestUri = requestUri;
	}

	public String[] getPathParams()
	{
		return pathParams;
	}

	public void setPathParams( String[] pathParams )
	{
		this.pathParams = pathParams;
	}
}